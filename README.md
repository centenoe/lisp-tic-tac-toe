Edgar Centeno
Tic Tac Toe Project

This project will make a tic tac toe game. The firt iteration will have a tic tac toe board, 
check if someone won the game and the ability to place a move on the board. The second iteration 
now has a playable Tic Tac Toe game.'x' will always be the first move, the game will stop once there
are three x's or o's in a row/column/cross. The Third iteration now includes AI, you can watch 
the AI play against itself or play against the AI as 'x or 'o. 

The software design document is called tttSDD.xml (PDF included) - I have not updated this since 
you said it is not required in class.

To run the program please make sure that EdgarCttt.lisp is in the same folder as the ccl interpreter
To run the game please type this: (load "EdgarCttt.lisp") 
To play the game please type: '(start-game)' after loading EdgarCttt.lisp 
You will be givin directions as how you would like to play the Tic Tac Toe game:

Enter 1 to play with two humans
-This will start the game in a blank board, enter a number between 0 and 8 to place move, x will always go first

Enter 2 to watch ai play eachother
-Ai will play the entire game, see what the AI has played

Enter 3 to play as 'x' against ai
-you will play against an AI and you will always go first as 'x

Enter 4 to play as 'o' against ai
-You will play against an AI but you will always go second as 'o 

Here is a numbered board:
=============
| 0 | 1 | 2 |
| 3 | 4 | 5 |
| 6 | 7 | 8 |
=============

