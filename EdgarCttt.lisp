;; EdgarCttt.lisp
;;
;; TicTacToe game iteration 3
;; Edgar Centeno
;; play the game by typing: "(start-game)" in ccl
;; Directions will be givin to you once you start the game.
;; Erik Steinmetz code is used
;;I used #|   |# for long comments, not sure about the comment etiquette of lisp

;; Here are the tests
(defparameter *brd* (list '- 'x '- 'o 'o 'o 'x '- '-))
(defparameter *crosswin* (list 'x 'x '- 'o 'x 'o 'x '- 'x))
(defparameter *full* (list 'x 'x 'x 'o 'x 'o 'x 'x 'x))
(defparameter *defult* (list '- '- '- '- '- '- '- '- '-))

;; Prints a tic-tac-toe board in a pretty fashion. By: Erik Steinmetz
;; Param: board - a list containing elements of a ttt board in row-major order
(defun print-board (board)
  (format t "=============")
  (do ((i 0 (+ i 1)) )
      ((= i 9) nil)
      (if (= (mod i 3) 0)
          (format t "~%|")
          nil)
      (format t " ~A |" (nth i board))
  )
  (format t "~%=============")
)

;; By: Erik Steinmetz
;; Tests whether all three items in a list are equal to each other
(defun threequal (list)
  (and (equal (first list) (second list))
       (equal (second list) (third list)))
)

;; By:Erik Steinmetz
;; Grabs the nth row of a tic-tac-toe board as a list
(defun grab-row (board row)
  (let ( (x (* 3 row)))
    (list (nth x board) (nth (+ 1 x) board) (nth (+ 2 x) board))
  )
)

;; By: Erik Steinmetz
;; Grabs the nth column of a tic-tac-toe board as a list
(defun grab-col (board col)
  (list (nth col board) (nth (+ 3 col) board) (nth (+ 6 col) board)))


;; This will take in a list(the tic tac toe board) and identify if there is a winner
;; Param: board - a list containing elements of a ttt board in row-major order
(defun winner (board)
;check if every row or collum is equal including the cross sections of the Board
;if any of them return true then there is a winner
(or (threequal(grab-row board 0)) (threequal(grab-row board 1)) (threequal(grab-row board 2))
    (threequal(grab-col board 0)) (threequal(grab-col board 1)) (threequal(grab-col board 2))
    ;the board is made up of 9 squares starting from 0
    ;every even number from 0 to 8 is in the cross section of the board
    ;(including 0)
    (and (equal (nth 0 board) (nth 4 board)) (equal (nth 4 board) (nth 8 board)) )
    (and (equal (nth 6 board) (nth 4 board)) (equal (nth 4 board) (nth 2 board)) )
))

;; Check if the move is legal
<<<<<<< HEAD
(defun check-legal (the-move board)
;;check if the input is infact a number
=======
;; Param: the-move - a number representing the move that the player has chosen
;; Param: board - a list containing elements of a ttt board in row-major order
(defun check-legal (the-move board)
;check if the input is infact a number
>>>>>>> working
  (and (integerp the-move)
  ;check if the input is more than -1 and less than 9
    (>= the-move 0)
    (<= the-move 8)
<<<<<<< HEAD
=======
    ;check that the move is part of an empty space.
>>>>>>> working
    (equal (nth the-move board) '-)
   )
)

;; This will place a move on the board and determin if that move is legal
;; Param: board - a list containing elements of a ttt board in row-major order
(defun place-move (board)
  (print "Enter move between 0 and 8")
  ;this will read the input from the user
  (setq the-move (read))
<<<<<<< HEAD
  ;; use function check-legal to check ifthe move is legal
  (if (check-legal the-move board)
  ;;if the input is correct then replace the position of the board with 'x'
=======
  ;use function check-legal to check ifthe move is legal
  (if (check-legal the-move board)
  ;if the input is correct then replace the position of the board with 'x'
>>>>>>> working
  ;(setf (nth the-move board)'x)
  (take-turn the-move board)
  ;else notify the user that the input is not valid
  (place-move board)
))

<<<<<<< HEAD
;;-------------------------------------------------------------
(defun ai-place-move (board)
  (print "Enter move between 0 and 8")
  ;;this will read the input from the user
  (setq the-move (random 9))
  ;; use function check-legal to check ifthe move is legal
  (if (check-legal the-move board)
  ;;if the input is correct then replace the position of the board with 'x'
  ;(setf (nth the-move board)'x)
  (take-turn the-move board)
  ;;else notify the user that the input is not valid
  (ai-place-move board)
))

;;----------------------

(defun ai-play-game (board)
 ;;print the board so the player know what to play
 (print-board board)
 (ai-place-move board)
  (if (>= (count-moves board) 5)
   (if (winner board)
       (who-won board)
       (ai-play-game board))
   (ai-play-game board))
)
;;------------------------------------------------------

;;This function will count the moves on the board
=======

;; This function will count the moves on the board
;; Param: board - a list containing elements of a ttt board in row-major order
>>>>>>> working
(defun count-moves (board)
;;if the board is null then return 0
(if (null board) 0
  ;;else if the board contains '-'add 1 to the count, if not then dont add
  ;;to the count
  (if (equal (car board)'-)
  (+ 0 (count-moves (cdr board)))
  (+ 1 (count-moves (cdr board))))))

;; This function will see whos turn it is to play
;; Param: the-move - a number representing the move that the player has chosen
;; Param: board - a list containing elements of a ttt board in row-major order
(defun take-turn (the-move board)
  ;;based on the moves on the board play a 'o or 'x
  (if(oddp (count-moves board))
     (setf (nth the-move board)'o)
     (setf (nth the-move board)'x)
     )
)

;; Find out who won
;; Param: board - a list containing elements of a ttt board in row-major order
(defun who-won (board)
  ;;if there is a winner detected find out how many moves there are on the Board
  ;;and determin who won
  (if (winner board)
      (if(oddp (count-moves board))
        (print "x has won!")
        (print "o has won!"))
      (place-move board)
  )
)

#|
this will play the game of tic tac toe
use the functions that work with eachother to play the game correctly
every time the total moves is less than 5 and there is no winner detected
make the player place a move.
I would have made a different function that would check for stalemates
but I think it make more sense to only check that when there are >=
5 moves in the board thus it would take the same amount of code to call it.
Here is the full function that I did not use seperately:
(defun full (board)
   (if (= (count-moves board) 9)
     (print "No one wins!")
     (print "the board is not full yet"))
)
|#
;; Param: board - a list containing elements of a ttt board in row-major order
(defun play-game (board)
 ;;print the board so the player know what to play
 (let (newboard) (setq newboard (copy-list board))
 (print-board newboard)
 (place-move newboard)
  (if (>= (count-moves newboard) 5)
   (if (winner newboard)
       (who-won newboard)
       ;checks if the board is full
       (if (= (count-moves newboard) 9)
            (print "No one wins!")
            (play-game newboard)))
  (play-game newboard)
)))

;; let ai place a move
;; Param: board - a list containing elements of a ttt board in row-major order
(defun ai-place-move (board)
  (print "Enter move between 0 and 8")
  (setq the-move (random 9))
  ;probably dont need to check since it will always be between 0 and 8
  (if (check-legal the-move board)
  (take-turn the-move board)
  (ai-place-move board)
))

;; ai plays the game essentially the same code as play-game
;; Param: board - a list containing elements of a ttt board in row-major order
(defun ai-play-game (board)
 ;;print the board so the player know what to play
 (let (newboard) (setq newboard (copy-list board))
 (print-board newboard)
 (ai-place-move newboard)
  (if (>= (count-moves newboard) 5)
   (if (winner newboard)
       (who-won newboard)
       (if (= (count-moves newboard) 9)
            (print "no one wins")
            (ai-play-game newboard)))
  (ai-play-game newboard)
)))

;; Simply check if the moves on the board are odd/even to see who places a move
;; Param: board - a list containing elements of a ttt board in row-major order
(defun player-ai-place-move (board)
  (if (oddp (count-moves board))
      (ai-place-move board)
      (place-move board)))

;; Player vursus ai (player plays first) (essentially same code as play-game)
;; Param: board - a list containing elements of a ttt board in row-major order
(defun player-v-ai-game (board)
  (let (newboard) (setq newboard (copy-list board))
  (print-board newboard)
  (player-ai-place-move newboard)
  (if (>= (count-moves newboard) 5)
    (if (winner newboard)
        (who-won newboard)
        (if (= (count-moves newboard) 9)
            (print "no one wins")
            (player-v-ai-game newboard)
          )
    )
  (player-v-ai-game newboard)
  )))

;; Simply check if the moves on the board are odd/even to see who places a move
;; Param: board - a list containing elements of a ttt board in row-major order
(defun ai-player-place-move (board)
  (if (oddp (count-moves board))
      (place-move board)
      (ai-place-move board)))

;; this is ai versus the player (ai plays first)
;; I know i could have made these "play game" functions better
;;(essentially the same code as play-game)
;; Param: board - a list containing elements of a ttt board in row-major order
(defun ai-v-player-game (board)
  (let (newboard) (setq newboard (copy-list board))
  (print-board newboard)
  (ai-player-place-move newboard)
  (if (>= (count-moves newboard) 5)
    (if (winner newboard)
       (who-won newboard)
       (if (= (count-moves newboard) 9)
          (print "No one wins!")
            (ai-v-player-game newboard))
     )
    (ai-v-player-game newboard)
  ))
)

;;This will check if the player chose a correct number to start the game
;; Param: mode-choice - A number that represents the type of game the player
;; wants to play.
(defun legal-start-game (mode-choice)
;check if the input is infact a number
  (and (integerp mode-choice)
  ;check if the input is between 1 and 4
    (>= mode-choice 1)
    (<= mode-choice 4)
   )
)



;;start the tic tac toe game. Player must chose a number between 1 and 4
;;if the number is not legal it will keep asking the questions.
(defun start-game ()
  (defparameter *defult* (list '- '- '- '- '- '- '- '- '-))
  (print "Enter 1 to play with two humans")
  (print "Enter 2 to watch ai play eachother")
  (print "Enter 3 to play as 'x' against ai")
  (print "Enter 4 to play as 'o' against ai")
  (print "Enter your choice here")
  (setq mode-choice (read))
  ;this is the chaning if statment that will check the mode chosen
  (if (legal-start-game mode-choice)
      (if(= mode-choice 1)
        (play-game *defult*)
        (if(= mode-choice 2)
        (ai-play-game *defult*)
        (if (= mode-choice 3)
        (player-v-ai-game *defult*)
        (if (= mode-choice 4)
        (ai-v-player-game *defult*)))
         )
         )
        (start-game)
  ))

#|
 I had a bug where it would say the wrong 'x' or 'o' had won
 after getting this to work I tested my old function
 I could not recreate the bug after completing the "fixed" version
 Below still works! but it is a lot harder to read

;;play the game. if the game is won with the minnimum amount of plays
;;display the correct winner
(defun play-game (board)
 print the board so the player know what to play
  (print-board board)
  (place-move board)
  (if (>= (count-moves board) 5)
   (if (= (count-moves board) 5)
     (if (winner board)
     (who-won-min board)
     (play-game board))
   (if (>= (count-moves board) 6)
     (if (winner board)
      (who-won board)
      (play-game board)
      )
     (play-game board)
    )
    )
    (play-game board)
    )
)


<<<<<<< HEAD
;(defun play-game (board)
 ;;print the board so the player know what to play
;  (print-board board)
;  (place-move board)
;  (if (>= (count-moves board) 5)
;   (if (= (count-moves board) 5)
;     (if (winner board)
;     (who-won-min board)
;     (play-game board))
;   (if (>= (count-moves board) 6)
;     (if (winner board)
;      (who-won board)
;      (play-game board)
;      )
;     (play-game board)
;    )
;    )
;    (play-game board)
;    )
;)


;(defun who-won-min (board)
  ;;if there is a winner detected find out how many moves there are on the Board
  ;;and determin who won
;  (if (winner board)
;      (if(oddp (count-moves board))
;        (print "o has won!")
;        (print "x has won!"))
;      (place-move board)
;  )
;)

;;(let (y) (setq y  (copy-list *defult*)) (print y))
=======
(defun who-won-min (board)
  if there is a winner detected find out how many moves there are on the Board
  and determin who won
  (if (winner board)
      (if(oddp (count-moves board))
        (print "o has won!")
        (print "x has won!"))
      (place-move board)
  )
)
|#
>>>>>>> working
